#!/bin/bash
#
# This script kills all other SSH sessions & leaves your current one intact;
# Tested & Works on Ubuntu (20.04.2) - Don't try it on anything else unless you have the capability to log back in.
#
# Remember: When applying this script, remove the brackets '[]' from the '[youruser]' entries on lines #17 & #20.
#
# Get off my damn server;
#
# Define targets & kill them;
  who -a | column -t | tail -n +3 | grep -v "id=" | grep -v ' \- ' | awk '{print $7}' | xargs kill 2>/dev/null
#
# Feature Request: https://twitter.com/tdreed00/status/1377177087358889984?s=20
  if [[ $? -eq "0" ]]; then
        if ! grep -q "AllowUsers" "/etc/ssh/sshd_config"; then
                echo "No AllowedUsers specified yet!"
                echo "AllowUsers [youruser]" >> /etc/ssh/sshd_config
        else
                echo "Adding [youruser] to the AllowUsers section"
                sed -i 's/AllowUsers /AllowUsers [youruser]/g' /etc/ssh/sshd_config
        fi
        # Reload SSH server;
        service ssh reload
  else
        echo "No users left to kill. You are the only one standing, senpai."
  fi
#
# Done;
  exit 0
